﻿using System;
using System.Collections.Generic;
using System.Linq;
using EYaIIS_Lab4.Entities;
using EYaIIS_Lab4.Helpers;

namespace EYaIIS_Lab4
{
  internal sealed class TextReader
  {
    private static readonly IEnumerable<string> NonVowelAlphabet = AppHelper.ReadNonVowelAlphabet();
    private static readonly IEnumerable<Letter> VowelAlphabet = AppHelper.ReadVowelLetters();
    private static readonly IEnumerable<string> Signs = AppHelper.ReadSigns();
    private static readonly IDictionary<string, int> TextSigns = AppHelper.ReadSignsText();

    public string Text { get; set; }

    public IEnumerable<Word> GetWords(string text)
    {
      if (string.IsNullOrEmpty(text))
      {
        throw new ArgumentNullException("text is null!");
      }

      var words = new List<Word>();

      var currentWord = new List<Letter>();
      foreach (var letter in text)
      {
        var currentLetter = letter.ToString();

        var vowelLetter = VowelAlphabet.FirstOrDefault(r => r.Name == currentLetter);

        if (vowelLetter != null)
        {
          currentWord.Add(vowelLetter);
        }
        else if (NonVowelAlphabet.Contains(currentLetter))
        {
          currentWord.Add(new Letter(currentLetter));
        }
        //else if (Signs.Contains(currentLetter))
        //{
        //  words.Add(new Word { Letters = currentWord });

        //  var whitespace = new List<Letter> { new Letter(" ") };
        //  words.Add(new Word { Letters = whitespace });
        //  words.Add(new Word { Letters = whitespace });
        //  currentWord = new List<Letter>();
        //}
        else if (TextSigns.ContainsKey(currentLetter))
        {
          words.Add(new Word { Letters = currentWord });

          var whitespace = new List<Letter> { new Letter(" ") };

          for (var i = 0; i < TextSigns[currentLetter]; i++)
          {
            words.Add(new Word {Letters = whitespace});
          }
          currentWord = new List<Letter>();
        }
        else
        {
          if (currentWord.Any())
          {
            if (string.IsNullOrEmpty(currentWord.ToString()) == false)
            {
              words.Add(new Word {Letters = currentWord});
            }

            var whitespace = new List<Letter> { new Letter(" ") };
            words.Add(new Word { Letters = whitespace });
            currentWord = new List<Letter>();
          }
        }
      }

      if (currentWord.Any() && string.IsNullOrEmpty(currentWord.ToString()) == false)
      {
        words.Add(new Word { Letters = currentWord });
      }

      return words;
    }
  }
}