﻿using System;
using System.Collections.Generic;
using System.IO;
using EYaIIS_Lab4.Entities;
using Newtonsoft.Json;

namespace EYaIIS_Lab4.Helpers
{
  public class AppHelper
  {
    public static IEnumerable<Letter> ReadVowelLetters()
    {
      try
      {
        string json = File.ReadAllText(@"AppSettings/VowelAlphabet.txt");
        var letters = JsonConvert.DeserializeObject<List<Letter>>(json);

        return letters;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    } 

    public static IEnumerable<string> ReadNonVowelAlphabet()
    {
      try
      {
      string[] lines = File.ReadAllLines(@"AppSettings/NonVowelAlphabet.txt");
      return lines;
      }
      catch
      {
        throw new FileNotFoundException("file NonVowelAlphabet.txt not found");
      }
    }

    public static IEnumerable<string> ReadSigns()
    {
      try
      {
        string[] lines = File.ReadAllLines(@"AppSettings/Signs1.txt");
        return lines;
      }
      catch
      {
        throw new FileNotFoundException("file Signs.txt not found");
      }
    }

    public static IEnumerable<CustomWord> ReadCustomWords()
    {
      try
      {
        string json = File.ReadAllText(@"AppSettings/CustomWords.txt");
        var words = JsonConvert.DeserializeObject<List<CustomWord>>(json);

        return words;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    } 

    public static IDictionary<string, string> ReadDefaultWords()
    {
      return ReadStringDictionary(@"AppSettings/CustomWords.txt");
    }

    public static IDictionary<string, string> ReadCustomPhonemes()
    {
      //return ReadStringDictionary(@"AppSettings/CustomPhonemes.txt");
      return new Dictionary<string, string>();
    }

    public static IDictionary<string, string> ReadDefaultPhonemes()
    {
      return ReadStringDictionary(@"AppSettings/DefaultPhonemes.txt");
    }

    public static IDictionary<string, int> ReadSignsText()
    {
      try
      {
        string json = File.ReadAllText(@"AppSettings/Signs.txt");

        var values = JsonConvert.DeserializeObject<Dictionary<string, int>>(json);
        return values;
      }
      catch (FileNotFoundException)
      {
        throw new FileNotFoundException(string.Format("file {0} not found", @"AppSettings/Signs.txt"));
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }

    #region privates

    private static IDictionary<string, string> ReadStringDictionary(string fileName)
    {
      try
      {
        string json = File.ReadAllText(fileName);

        var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        return values;
      }
      catch (FileNotFoundException)
      {
        throw new FileNotFoundException(string.Format("file {0} not found", fileName));
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }

    #endregion
  }
}