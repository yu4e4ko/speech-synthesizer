﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using EYaIIS_Lab4.Entities;
using NAudio.Wave;

namespace EYaIIS_Lab4
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private const string ResultFilePath = "Output/result.wav";

    private WaveFileReader _waveReader;
    private DirectSoundOut _output;
    private string _phonemesDirName;
    private TextReader _textReader;
    private PhonemesBuilder _phonemesCreator;

    public MainWindow()
    {
      InitializeComponent();
      Title = string.Empty;

      ResizeMode = ResizeMode.NoResize;
      Init();
    }

    #region events

    private void Init()
    {
      try
      {
        //_phonemesDirName = "Phonemes";
        //_phonemesDirName = "PhonemesDasha";
        //_phonemesDirName = "PhonemesNikita";
        //_phonemesDirName = "PhonemesLesha";
        _phonemesDirName = "PhonemesNastya";
        _textReader = new TextReader();
        _phonemesCreator = new PhonemesBuilder();
      }
      catch (Exception ex)
      {
        MessageBox.Show(string.Format("App init error: {0}", ex.Message));
        Environment.Exit(1);
      }
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      txtMainText.Text = string.Empty;
    }

    private void btnPlay_Click(object sender, RoutedEventArgs e)
    {
      var text = txtMainText.Text;

      if (string.IsNullOrEmpty(text))
      {
        return;
      }

      var fullPhonemes = new List<string>();

      try
      {
        var words = _textReader.GetWords(text.ToLower());
        //MessageBox.Show(WordsToString(words));

        foreach (var word in words)
        {
          var phonemes = _phonemesCreator.BuildPhonemes(word).ToList();      
          //ShowPhonemes(phonemes);      
          fullPhonemes.AddRange(phonemes);
        }

        MessageBox.Show("Success");
        GenerateWavAndPlayFile(fullPhonemes);
        MessageBox.Show("End");
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message);
      }
    }

    #endregion

    #region privates

    private void GenerateWavAndPlayFile(IEnumerable<string> phonemesNames)
    {
      if (_waveReader != null)
      {
        _waveReader.Dispose();
      }
      if (_output != null)
      {
        _output.Dispose();
      }

      using (var waveWriter = new WaveFileWriter(ResultFilePath, new WaveFormat()))
      {
        foreach (var phonemesName in phonemesNames)
        {
          var fullPath = GetFullPath(phonemesName);

          using (var currentWaveFileReader = new WaveFileReader(fullPath))
          {
            var length = (int) currentWaveFileReader.Length;
            var buffer = new byte[length];
            currentWaveFileReader.Read(buffer, 0, length);

            waveWriter.Write(buffer,0, length);
          }
        }
      }

      _waveReader = new WaveFileReader(ResultFilePath);
      _output = new DirectSoundOut();
      _output.Init(new WaveChannel32(_waveReader));
      _output.Play();
    }

    private string GetFullPath(string phonemeName)
    {
      if (string.IsNullOrEmpty(_phonemesDirName))
      {
        return phonemeName;
      }

      return string.Format("{0}/{1}", _phonemesDirName, phonemeName);
    }


    /// <summary>
    /// HELPERS
    /// </summary>
    private static void ShowPhonemes(IEnumerable<string> phonemes)
    {
      var result = string.Empty;

      foreach (var phoneme in phonemes)
      {
        if (phoneme == "space.wav")
        {
          break;
        }
        result += phoneme + "\n";
      }

      if (string.IsNullOrEmpty(result) == false)
      {
        MessageBox.Show(result);
      }
    }

    private static string WordsToString(IEnumerable<Word> words)
    {
      var result = string.Empty;

      foreach (var word in words)
      {
        result = word.Letters.Aggregate(result, (current, letter) => current + letter.Name);
        result +=  "\n";
      }

      return result;
    }

    #endregion
  }
}
