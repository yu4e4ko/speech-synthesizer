﻿using Newtonsoft.Json;

namespace EYaIIS_Lab4
{
  public class Letter
  {
    public Letter(string name)
    {
      Name = name;
    }

    public Letter()
    {
    }

    [JsonProperty("Name")]
    public string Name { get; set; }

    [JsonProperty("IsVowel")]
    public bool IsVowel { get; set; }

    [JsonProperty("IsSimple")]
    public bool IsSimple { get; set; }

    [JsonProperty("Phoneme")]
    public string Phoneme { get; set; }
  }
}
