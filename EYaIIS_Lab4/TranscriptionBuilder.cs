﻿using System.Collections.Generic;
using System.Linq;
using EYaIIS_Lab4.Entities;

namespace EYaIIS_Lab4
{
  public class TranscriptionBuilder
  {
    private readonly IDictionary<string, string> _sounds = new Dictionary<string, string>
    {
      {"б", "п"},
      {"г", "х"},
      {"д", "т"},
      {"з", "с"}
    }; 

    public string BuildTranscription(Word word)
    {
      CheckFirstU(word);

      var transcription =  CreatePhonemesString(word);

      transcription = ReplaceDoubleLetters(transcription);

      return transcription;
    }

    private string CreatePhonemesString(Word word)
    {
      var phonemesString = string.Empty;

      if (word.Accent == 0)
      {
        AddAccentIfNotExist(word);
      }

      var letters = word.Letters;
      var phonemes = new List<string>();

      for (var i = 0; i < letters.Count; i++)
      {
        var currentLetter = letters[i];
        if (currentLetter.IsVowel)
        {
          if (currentLetter.IsSimple == false)
          {
            if (i == 0)
            {
              phonemes.Add("0й");
              CheckAccentAndAdd(phonemes, word, i, currentLetter.Phoneme);
            }
            else if (i != 0 && letters[i - 1].IsVowel)
            {
              phonemes.Add("0й");
              CheckAccentAndAdd(phonemes, word, i, currentLetter.Phoneme);
            }
            else
            {
              CheckAccentAndAdd(phonemes, word, i, currentLetter.Phoneme);
            }
          }
          else
          {
            CheckAccentAndAdd(phonemes, word, i, currentLetter.Phoneme);
          }
        }
        else
        {
          if (letters.Count() - 1 > i && letters[i + 1].IsVowel && (letters[i + 1].IsSimple == false || letters[i + 1].Name == "і"))
          {
            phonemes.Add("1" + currentLetter.Name);
          }
          else if (letters.Count() - 1 == i && _sounds.ContainsKey(currentLetter.Name))
          {
            phonemes.Add("0" + _sounds[currentLetter.Name]);
          }
          else
          {
            phonemes.Add("0" + currentLetter.Name);
          }

          if (i != 0)
          {
            if (currentLetter.Name == "ь")
            {
              var last = phonemes[i - 1];
              phonemes[i - 1] = last.Replace("0", "1");
            }
          }
        }
      }

      for (var i = 0; i < phonemes.Count; i++)
      {
        phonemesString += phonemes[i];

        if (i < phonemes.Count - 1)
        {
          phonemesString += "_";
        }
      }

      phonemesString = phonemesString.Replace("_0ь", "");

      return phonemesString;
    }

    private void CheckAccentAndAdd(List<string> phonemes, Word word, int currentIndex, string phoneme)
    {
      if (currentIndex + 1 == word.Accent)
      {
        phonemes.Add("1" + phoneme);
      }
      else
      {
        phonemes.Add("0" + phoneme);
      }
    }

    private void AddAccentIfNotExist(Word word)
    {
      for (var i = 0; i < word.Letters.Count; i++)
      {
        if (word.Letters[i].IsVowel)
        {
          word.Accent = i + 1;
          return;
        }
      }
    }

    private void CheckFirstU(Word word)
    {
      if (word.Letters.First().Name == "ў")
      {
        word.Letters[0].Name = "у";
      }
    }

    private string ReplaceDoubleLetters(string transcription)
    {
      var dz = "0д_1з";
      var cc = "0ц_0ц";
      var dzh = "0д_0ж";
      var nn1 = "0н_1н";

      transcription = transcription.Replace(dz, "1дз");
      transcription = transcription.Replace(cc, "0цц");
      transcription = transcription.Replace(dzh, "0дж");
      transcription = transcription.Replace(nn1, "1нн");

      return transcription;
    }
  }
}