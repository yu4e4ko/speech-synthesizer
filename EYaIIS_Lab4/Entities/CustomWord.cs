﻿namespace EYaIIS_Lab4.Entities
{
  public class CustomWord
  {
    public string Text { get; set; }

    public int Accent { get; set; }

    public string PartOfSpeach { get; set; }
  }
}