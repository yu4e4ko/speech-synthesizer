﻿using System;
using System.Collections.Generic;
using System.Linq;
using EYaIIS_Lab4.Helpers;


namespace EYaIIS_Lab4
{
  public class PhonemesCreator
  {
    private const string DefaultPhoneme = "space.wav";

    private static readonly IDictionary<string, string> CustomWords = AppHelper.ReadDefaultWords();
    private static readonly IDictionary<string, string> CustomPhonemes = AppHelper.ReadCustomPhonemes();
    private static readonly IDictionary<string, string> DefaultPhonemes = AppHelper.ReadDefaultPhonemes();

    public IEnumerable<string> CreatePhonemes(Word word)
    {
      if (word.Letters.Count == 1 && word.Letters.FirstOrDefault().Name == " ")
      {
        return new List<string> { "space.wav"};
      }

      var phonemesString = CustomWords.ContainsKey(word.ToString()) ? CustomWords[word.ToString()] : CreatePhonemesString(word);

      var phonemesPath = BuildPhonemesPath(phonemesString);
      return phonemesPath;
    }

    private string CreatePhonemesString(Word word)
    {
      var phonemesString = string.Empty;
      var letters = word.Letters;
      var phonemes = new List<string>();

      for (var i = 0; i < letters.Count; i++)
      {
        var currentLetter = letters[i];
        if (currentLetter.IsVowel)
        {
          if (currentLetter.IsSimple == false)
          {
            if (i == 0)
            {
              phonemes.Add("0й");
              phonemes.Add("1" + currentLetter.Phoneme);
            }
            else if (i != 0 && letters[i - 1].IsVowel)
            {
              phonemes.Add("0й");
              phonemes.Add("1" + currentLetter.Phoneme);
            }
            else
            {
              phonemes.Add("1" +currentLetter.Phoneme);
            }
          }
          else
          {
            phonemes.Add("1" + currentLetter.Phoneme);
          }
        }
        else
        {
          if (letters.Count() - 1 > i && letters[i + 1].IsVowel && letters[i + 1].IsSimple == false)
          {
            phonemes.Add("1" + currentLetter.Name);
          }
          else
          {
            phonemes.Add("0" + currentLetter.Name);
          }

          if (i != 0)
          {
            if (currentLetter.Name == "ь")
            {
              var last = phonemes.Last();
              phonemes[i] = last.Replace("0", "1");
            }
          }
        }
      }

      for (var i = 0; i < phonemes.Count; i++)
      {
        phonemesString += phonemes[i];

        if (i < phonemes.Count - 1)
        {
          phonemesString += "_";
        }
      }

      return phonemesString;
    }

    private IEnumerable<string> BuildPhonemesPath(string phonemesString)
    {
      string[] phonemes = null;

      if (CustomPhonemes.ContainsKey(phonemesString))
      {
        var value = CustomPhonemes[phonemesString];
        phonemesString = phonemesString.Replace(phonemesString, value);
      }
      else
      {
        foreach (var keyValue in CustomPhonemes)
        {
          if (phonemesString.Contains(keyValue.Key))
          {
            phonemesString = phonemesString.Replace(keyValue.Key, keyValue.Value);
          }
        }
      }

      phonemes = phonemesString.Split('_');

      for(var i = 0; i < phonemes.Length; i++)
      {
        if (phonemes[i].Contains(".wav") == false)
        {
          if (DefaultPhonemes.ContainsKey(phonemes[i]))
          {
            phonemes[i] = DefaultPhonemes[phonemes[i]];
          }
          else
          {
            throw new Exception(string.Format("\"{0}\" phoneme not found", phonemes[i]));
            //phonemes[i] = DefaultPhoneme;
          }
        }
      }

      return phonemes;
    }
  }
}