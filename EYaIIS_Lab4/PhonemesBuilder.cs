﻿using System;
using System.Collections.Generic;
using System.Linq;
using EYaIIS_Lab4.Entities;
using EYaIIS_Lab4.Helpers;


namespace EYaIIS_Lab4
{
  public class PhonemesBuilder
  {
    private const string SpacePhoneme = "space.wav";

    //private static readonly IDictionary<string, string> AbsoleteCustomWords = AppHelper.ReadDefaultWords();
    private static readonly IDictionary<string, string> CustomPhonemes = AppHelper.ReadCustomPhonemes();
    private static readonly IDictionary<string, string> DefaultPhonemes = AppHelper.ReadDefaultPhonemes();
    private static readonly IEnumerable<CustomWord> CustomWords = AppHelper.ReadCustomWords(); 

    public IEnumerable<string> BuildPhonemes(Word word)
    {
      if (word.Letters.Any() == false)
      {
        return new List<string> { SpacePhoneme };
      }

      if (word.Letters.Count == 1 && word.Letters.FirstOrDefault().Name == " " || string.IsNullOrEmpty(word.Letters.FirstOrDefault().Name))
      {
        return new List<string> { SpacePhoneme };
      }

      SetAccents(word);

      var phonemesString = new TranscriptionBuilder().BuildTranscription(word);

      var phonemesPath = BuildPhonemesPath(phonemesString);
      return phonemesPath;
    }

    private void SetAccents(Word word)
    {
      var customWord = CustomWords.FirstOrDefault(r => r.Text.Equals(word.ToString(), StringComparison.CurrentCultureIgnoreCase));

      if (customWord == null)
      {
        return;
      }

      word.Accent = customWord.Accent;
      word.PartOfSpeach = customWord.PartOfSpeach;
    }

    private IEnumerable<string> BuildPhonemesPath(string phonemesString)
    {
      string[] phonemes = null;

      if (CustomPhonemes.ContainsKey(phonemesString))
      {
        var value = CustomPhonemes[phonemesString];
        phonemesString = phonemesString.Replace(phonemesString, value);
      }
      else
      {
        foreach (var keyValue in CustomPhonemes)
        {
          if (phonemesString.Contains(keyValue.Key))
          {
            phonemesString = phonemesString.Replace(keyValue.Key, keyValue.Value);
          }
        }
      }

      phonemes = phonemesString.Split('_');

      for(var i = 0; i < phonemes.Length; i++)
      {
        if (phonemes[i].Contains(".wav") == false)
        {
          if (DefaultPhonemes.ContainsKey(phonemes[i]))
          {
            phonemes[i] = DefaultPhonemes[phonemes[i]];
          }
          else if (phonemes[i] == "0ь")
          {
            // nothing
          }
          else
          {
            throw new Exception(string.Format("\"{0}\" phoneme not found", phonemes[i]));
            //phonemes[i] = DefaultPhoneme;
          }
        }
      }

      return phonemes;
    }
  }
}