﻿using System.Collections.Generic;

namespace EYaIIS_Lab4
{
  public class Word
  {
    public Word()
    {
      Letters = new List<Letter>();
    }

    public IList<Letter> Letters { get; set; }

    public override string ToString()
    {
      var result = string.Empty;

      foreach (var letter in Letters)
      {
        result += letter.Name;
      }

      return result;
    }
  }
}
